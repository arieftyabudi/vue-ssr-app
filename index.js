
import express from 'express'
import { createSSRApp } from 'vue'
import {renderToString} from 'vue/server-renderer'

const server = express()
const port =3000

server.get('/',(req,res)=>{
    const app = createSSRApp({

        data:()=>({
            name:'Pemuda Js'
        }),
        template:`
        <div>
        <h1>Hello World! {{name}}</h1>
        <p>Welcome to my page</p>
        <h3>Another Page</h3>
        
        <ul>
            <li><a href ='/about'>About</a></li>
            <li><a href ='/contact'>Contact</a></li>
        </ul>
        </div>
    `
    })
    
    renderToString(app).then((html)=>{
       
        res.send(`
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>
            <body>
             ${html}
            <body>
        </html>
        `)
    })
})

server.get('/about',(req, res)=> {
    const about = createSSRApp({
        template:`<div> 
        <h1>Hello World!</h1>
        <p>Welcome to about page</p>
        <div>`
    })

    renderToString(about).then((html)=>{
        res.send(`
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Page</title>
</head>
<body>
   ${html}
    </body>
</html>
        `)
    })
  })

  server.get('/contact',(req,res)=>{
    const contact = createSSRApp({
        data:()=>({
            name:'Pemuda JS'
        }),
     template: `<div>
    <h1>Hello World!</h1>
    <p>Welcome to contact page {{name}}</p>
    </div>
    `
    })
    renderToString(contact).then((html)=>{
        res.send(`
        <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Contact Page</title>
        </head>
        <body>
            ${html}
        </body>
    </html>
        `)
    })
  })

server.listen(port,()=>{
    console.log(`Server On Port ${port}`)
})